//package com.weaver.tools.workflow;
//
//import cn.hutool.core.codec.Base64;
//import cn.hutool.core.map.CaseInsensitiveMap;
//import cn.hutool.core.util.StrUtil;
//import cn.hutool.http.HttpRequest;
//import cn.hutool.http.HttpResponse;
//import cn.hutool.http.HttpUtil;
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.google.common.collect.Maps;
//import com.weaver.workflow.common.constant.event.WorkflowEvent;
//import com.weaver.workflow.common.entity.core.flow.WorkflowExeEventListenerResult;
//import com.weaver.workflow.common.entity.core.flow.WorkflowRequestEntityInterface;
//import com.weaver.workflow.core.cfg.delegate.WorkflowExeListener;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.Cookie;
//import java.util.Map;
//
///**
// * 流程监听基础类
// */
//@Slf4j
//public abstract class BaseWorkflowListener implements WorkflowExeListener {
//
//    protected String dataGroupId;
//    protected String mainTable;
//
//    public void setDataGroupId(String dataGroupId) {
//        this.dataGroupId = dataGroupId;
//    }
//
//    public void setMainTable(String mainTable) {
//        this.mainTable = mainTable;
//    }
//
//    protected String getOrigin() {
//        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        return sra.getRequest().getHeader("origin");
//    }
//
//    /**
//     * 获取当前请求的用户cookie信息
//     */
//    protected String getCookie() {
//        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
//        StringBuilder cookieHeaderStr = new StringBuilder();
//        Cookie[] cookies = sra.getRequest().getCookies();
//        Cookie[] var9 = cookies;
//        int var10 = cookies.length;
//
//        for(int var11 = 0; var11 < var10; ++var11) {
//            Cookie cookie = var9[var11];
//            cookieHeaderStr.append(String.format("%s=%s; ", cookie.getName(), cookie.getValue()));
//        }
//        return cookieHeaderStr.toString();
//    }
//
//    protected String encodeSql(String sql) {
//        // 对sql 进行base64
//        return Base64.encode(sql);
//    }
//
//    protected HttpResponse execSql(Map<String, Object> params) {
//        String origin = getOrigin();
//        HttpRequest request = HttpUtil.createPost(origin + "/api/datasource/ds/executeSql");
//        request.form(params);
//        // 添加请求信息
//        Map<String, String> headers = Maps.newHashMap();
//        headers.put("Cookie", getCookie());
//        request.addHeaders(headers);
//        HttpResponse response = request.execute();
//        return response;
//    }
//
//    /**
//     * 根据requestId 查询主表数据
//     * @return
//     */
//    protected Map<String, String> queryMainData(long requestId) {
//        CaseInsensitiveMap<String, String> data = new CaseInsensitiveMap<>();
//        Map<String, Object> params = Maps.newHashMap();
//        params.put("sourceType", "EXTERNAL");
//        params.put("groupId", dataGroupId);
//        params.put("sql", encodeSql(String.format("select * from %s where FORM_DATA_ID = (select dataid from wfc_form_data where requestid = %s)",
//                mainTable, requestId)));
//        HttpResponse response = execSql(params);
//        if (response.isOk()) {
//            JSONObject res = JSON.parseObject(response.body());
//            if (res.getInteger("code") == 200) {
//                JSONArray dataArr = res.getJSONObject("data").getJSONArray("records");
//                if (dataArr.size() > 0) {
//                    JSONObject jsonObj = dataArr.getJSONObject(0);
//                    for (Map.Entry<String, Object> stringObjectEntry : jsonObj.entrySet()) {
//                        data.put(stringObjectEntry.getKey(), StrUtil.toString(stringObjectEntry.getValue()));
//                    }
//                } else {
//                    return data;
//                }
//            } else {
//                log.error("主表数据信息查询失败 => {}", res.toJSONString());
//            }
//        } else {
//            log.error("主表数据信息查询异常 => {}", response.body());
//        }
//        return data;
//    }
//
//    public abstract WorkflowExeEventListenerResult execCode(WorkflowEvent eventType, WorkflowRequestEntityInterface requestEntity, Map<String, Object> customPropertiesMap) throws Exception;
//
//    public WorkflowExeEventListenerResult notify(WorkflowEvent eventType, WorkflowRequestEntityInterface requestEntity, Map<String, Object> customPropertiesMap) {
//        WorkflowExeEventListenerResult result = new WorkflowExeEventListenerResult(true);
//        try {
//            return execCode(eventType, requestEntity, customPropertiesMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
//}
