package com.weaver.dev.tools;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Map;

/**
 * @Description
 * @Author Qfengx <qffg1998@126.com>
 * @Date 2021-09-11 18:27:25
 */
public class CollectionUtils {

    public static boolean isEmpty(@Nullable Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }

    public static boolean isEmpty(@Nullable Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }

}
