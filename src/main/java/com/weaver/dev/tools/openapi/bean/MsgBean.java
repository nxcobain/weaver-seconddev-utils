package com.weaver.dev.tools.openapi.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class MsgBean {

    private String text;
    private String title;
    private Map<String ,Object> sender;
    private int eventId;
    private int moduleId;
    private List<Map<String, Object>> recivers;
    private Map<String, Object> entity;
    private String id;
    private String name;
    private String pcUrl;
    private String h5Url;

}
