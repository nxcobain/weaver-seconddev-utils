package com.weaver.dev.tools.openapi.config;

/**
 * E10 open api 配置类
 */
public class WeaverOpenApiConfig {

    public String getHostUrl() {
        return "https://api.eteams.cn";
    }

    public String getAuthorizeUrl() {
        return getHostUrl() + "/oauth2/authorize";
    }

    public String getAccessTokenUrl() {
        return getHostUrl() + "/oauth2/access_token";
    }

    public String getLoginTokenUrl() {
        return getHostUrl() + "/oauth2/get_logintoken";
    }

    public String getQueryEventsUrl() {
        return getHostUrl() + "/room/v2/queryEvents";
    }

    public String getQueryBlogUrl() {
        return getHostUrl() + "/blog/v2/queryBlog";
    }

    public String getWorkflowCreateUrl() {
        return getHostUrl() + "/workflow/v2/create";
    }

    public String getWorkflowFieldIdUrl() {
        return getHostUrl() + "/workflow/v2/getFieldById";
    }

    public String getSendMsgUrl() {
        return getHostUrl() + "/api/mc/msg/sendMsg";
    }

}
