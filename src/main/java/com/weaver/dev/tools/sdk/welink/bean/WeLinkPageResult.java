package com.weaver.dev.tools.sdk.welink.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * welink 分页类结果类
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WeLinkPageResult {

    private String code;
    private String message;
    private Integer limit;
    private Integer offset;
    private Integer totalCount;
    private List<AttendanceRecordsBean> data;

}
