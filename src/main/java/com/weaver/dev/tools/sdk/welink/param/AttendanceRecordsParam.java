package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceRecordsParam {

    private Integer offset;
    private Date startTime;
    private Date endTime;
    private Integer limit;
    private List<String> userIdList;

}
