package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class CreateConferenceParam {

    private Integer conferenceType;
    private String startTime;
    private Integer length;
    private String subject;
    private String mediaTypes;
    private List<RestAttendeeDTO> attendees;
    private Integer isAutoRecord;
    private Integer encryptMode;
    private String language;
    private String timeZoneID;
    private Integer recordType;
    private String liveAddress;
    private String auxAddress;
    private Integer recordAuxStream;
    private RestConfConfigDTO confConfigInfo;
    private Integer vmrFlag;
    private Integer vmrID;

}
