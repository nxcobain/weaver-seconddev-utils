package com.weaver.dev.tools.sdk.moxueyuan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 魔学院用户实体类
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class MxyUserEntity {

    private String userid;
    private String name;

    private List<String> department;

}
