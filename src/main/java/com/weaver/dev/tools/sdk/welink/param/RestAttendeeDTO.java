package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class RestAttendeeDTO {

    private String accountId;
    private String name;
    private Integer role;
    private String phone;
    private String phone2;
    private String phone3;
    private String email;
    private String sms;
    private Integer isAutoInvite;
    private String type;
    private String address;
    private String deptName;

}
