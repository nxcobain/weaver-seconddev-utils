package com.weaver.dev.tools.sdk.moxueyuan.config;

/**
 * 魔学院SDK 配置类
 */
public class MxyConfig {

    protected String protocol = "https";

    protected String domain = "open.moxueyuan.com";

    public MxyConfig() { }

    public MxyConfig(String protocol, String domain) {
        this.protocol = protocol;
        this.domain = domain;
    }

    public String getProtocol() { return this.protocol; }

    public String getDomain() { return this.domain; }

    public String getHost() { return this.domain; }

    public String getHostUrl() {
        return String.format("%s://%s", getProtocol(), getDomain());
    }

    public String getAccessTokenUrl() {
        return getHostUrl() + "/api/v1/connect/get-token";
    }

    public String getUseridsUrl() {
        return getHostUrl() + "/api/v1/contacts/user/simplelist-userids";
    }

    public String getDeptListUser() {
        return getHostUrl() + "/api/v1/contacts/department/lists";
    }

    public String getDeptIds() {
        return getHostUrl() + "/api/v1/contacts/department/list-ids";
    }

    public String getAddUserUrl() {
        return getHostUrl() + "/api/v1/contacts/user/create";
    }

    public String getSsoLoginUrl() {
        return getHostUrl() + "/api/v1/sso/login";
    }

}
