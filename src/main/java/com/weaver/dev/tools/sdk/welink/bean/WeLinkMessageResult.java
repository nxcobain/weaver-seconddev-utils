package com.weaver.dev.tools.sdk.welink.bean;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WeLinkMessageResult {

    private String code;
    private String message;
    private List<String> failedUserId;
    private String itemId;

    public boolean isOk() {
        return StrUtil.equals(code, "0");
    }

}
