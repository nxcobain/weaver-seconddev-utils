package com.weaver.dev.tools.sdk.kuaidi100;

import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.weaver.dev.tools.sdk.kuaidi100.config.Kuaidi100Config;
import com.weaver.dev.tools.sdk.kuaidi100.entity.PollQueryResult;
import com.weaver.dev.tools.http.RequestUtils;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import java.io.IOException;
import java.util.Map;

/**
 * 快递100 SDK集成
 */
public class Kuaidi100Sdk {

    private String accessKey;
    private String customer;
    private String secret ;
    private Kuaidi100Config config;
    private OkHttpClient client;

    public Kuaidi100Sdk(String accessKey, String customer, String secret) {
        this(accessKey, customer, secret, new Kuaidi100Config(), new OkHttpClient());
    }

    public Kuaidi100Sdk(String accessKey, String customer, String secret, OkHttpClient client) {
        this(accessKey, customer, secret, new Kuaidi100Config(), client);
    }

    public Kuaidi100Sdk(String accessKey, String customer, String secret, Kuaidi100Config config) {
        this(accessKey, customer, secret, config, new OkHttpClient());
    }


    public Kuaidi100Sdk(String accessKey, String customer, String secret, Kuaidi100Config config, OkHttpClient client) {
        this(accessKey, customer, secret, true, config, client);
    }

    public Kuaidi100Sdk(String accessKey, String customer, String secret, boolean isLazy, Kuaidi100Config config, OkHttpClient client) {
        this.accessKey = accessKey;
        this.customer = customer;
        this.secret = secret;
        this.config = config;
        this.client = client;
    }

    public String createSign(String param) {
        String data = param + accessKey + customer;
        return SecureUtil.md5(data).toUpperCase();
    }

    public PollQueryResult pollQuery(String com, String num) throws IOException {
        Map<String, Object> param = Maps.newHashMap();
        param.put("com", com);
        param.put("num", num);
        String paramStr = JSON.toJSONString(param);
        String sign = createSign(paramStr);
        Map<String, String> realParam = Maps.newHashMap();
        realParam.put("param", paramStr);
        realParam.put("sign", sign);
        realParam.put("customer", customer);

        Response response = RequestUtils.doPost(client, config.getPollQueryUrl(), Maps.newHashMap(), realParam);
        if (response.isSuccessful()) {
            return JSON.parseObject(response.body().string(), PollQueryResult.class);
        } else {
            throw new RuntimeException("请求异常: " + response.body().string());

        }
    }


    public static void main(String[] args) throws IOException {
//        Kuaidi100Sdk kuaidi100Sdk = new Kuaidi100Sdk("zdomqmHd1955", "E999701BACA309274FBE276EE7C4C42C", "a236b63eff7a4753b9f6bdb448c2b42b");
//        String s = "{\"com\":\"shentong\",\"num\":\"777094990948443\",\"phone\":\"\",\"from\":\"\",\"to\":\"\",\"resultv2\":0,\"show\":\"0\",\"order\":\"desc\"}"
//        System.out.println(kuaidi100Sdk.pollQuery("shentong", "777094990948443"));
    }

}
