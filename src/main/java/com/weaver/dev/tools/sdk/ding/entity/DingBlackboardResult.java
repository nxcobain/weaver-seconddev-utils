package com.weaver.dev.tools.sdk.ding.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingBlackboardResult {

    private Integer errcode;
    private String errmsg;
    private Boolean result;
    private Boolean success;
    private String request_id;

    public boolean isOk() {
        return errcode == 0;
    }

}
