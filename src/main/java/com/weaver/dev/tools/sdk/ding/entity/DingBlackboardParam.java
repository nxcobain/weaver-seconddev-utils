package com.weaver.dev.tools.sdk.ding.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingBlackboardParam {

    private String operation_userid;
    private String author;
    private Integer private_level;
    private Boolean ding;
    private Map<String, Object> blackboard_receiver;
    private String title;
    private Boolean push_top;
    private String content;
    private String category_id;
    private String coverpic_mediaid;


}
