package com.weaver.dev.tools.sdk.ding.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingTaskParam {

    private String sourceId;
    private String subject;
    private String creatorId;
    private String description;
    private Long dueTime;
    private List<String> executorIds;
    private List<String> participantIds;
    private Map<String, Object> detailUrl;
    private Boolean isOnlyShowExecutor;
    private Integer priority;
    private Object notifyConfigs;

}
