package com.weaver.dev.tools.sdk.moxueyuan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class MxyResult {

    private String status;
    private String errmsg;
    private Integer errcode;

    private Object results;

}
