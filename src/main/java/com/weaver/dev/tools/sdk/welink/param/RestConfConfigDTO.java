package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class RestConfConfigDTO {

    private Boolean isGuestFreePwd;
    private Boolean isSendNotify;
    private Boolean isSendSms;


}
