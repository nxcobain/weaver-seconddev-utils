package com.weaver.dev.tools.sdk.ding.config;

public class DingConfig {
    protected String protocol = "https";

    protected String domain = "oapi.dingtalk.com";

    public DingConfig() { }

    public DingConfig(String protocol, String domain) {
        this.protocol = protocol;
        this.domain = domain;
    }

    public String getProtocol() { return this.protocol; }

    public String getDomain() { return this.domain; }

    public String getHost() { return this.domain; }

    public String getHostUrl() {
        return String.format("%s://%s", getProtocol(), getDomain());
    }

    public String getAccessTokenUrl() {
        return getHostUrl() + "/gettoken";
    }

    public String getSendCorpMessageUrl() {
        return getHostUrl() + "/topapi/message/corpconversation/asyncsend_v2";
    }

    public String getCreateBlackboardUrl() {
        return getHostUrl() + "/topapi/blackboard/create";
    }

    public String getAddTodoTaskUrl(String unionid) {
        return getHostUrl() + "/v1.0/todo/users/" + unionid + "/tasks";
    }

    public String getCreateVideoMeetingUrl() {
        return getHostUrl() + "/v1.0/conference/videoConferences";
    }

    public String getAttendUploadUrl() {
        return getHostUrl() + "/topapi/attendance/record/upload";
    }
//
    public String getCreateCalendarUrl(String userId, String calendarId) {
        return getHostUrl() + "/v1.0/calendar/users/" + userId + "/calendars/" + calendarId + "/events";
    }
}
