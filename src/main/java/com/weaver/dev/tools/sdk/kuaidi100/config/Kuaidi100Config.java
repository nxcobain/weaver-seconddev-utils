package com.weaver.dev.tools.sdk.kuaidi100.config;

public class Kuaidi100Config {

    protected String protocol = "https";

    protected String domain = "poll.kuaidi100.com";

    public Kuaidi100Config() { }

    public Kuaidi100Config(String protocol, String domain) {
        this.protocol = protocol;
        this.domain = domain;
    }

    public String getProtocol() { return this.protocol; }

    public String getDomain() { return this.domain; }

    public String getHost() { return this.domain; }

    public String getHostUrl() {
        return String.format("%s://%s", getProtocol(), getDomain());
    }

    public String getPollQueryUrl() {
        return getHostUrl() + "/poll/query.do";
    }


}
