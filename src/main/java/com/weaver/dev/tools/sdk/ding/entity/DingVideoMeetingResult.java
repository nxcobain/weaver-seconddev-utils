package com.weaver.dev.tools.sdk.ding.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingVideoMeetingResult {

    private String conferenceId;
    private String conferencePassword;
    private String hostPassword;
    private String externalLinkUrl;
    private List<String> phoneNumbers;
    private String requestId;

}
