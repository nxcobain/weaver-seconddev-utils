package com.weaver.dev.tools.sdk.ding.entity;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingCalendarCreateParam {

    private String summary;
    private String description;
    private Map<String, Object> start;
    private Map<String, Object> end;
    private boolean isAllDay;
    private Map<String, Object> recurrence;
    private List<Map<String, Object>> attendees;
    private Map<String, Object> location;
    private List<Map<String, Object>> reminders;
    private Map<String, Object> onlineMeetingInfo;
    private Map<String, Object> extra;

    public static Map<String, Object> getDateTimeMap(String dateStr, boolean isAllDay) {
        return getDateTimeMap(dateStr, isAllDay, TimeZone.getDefault());
    }

    public static Map<String, Object> getDateTimeMap(String dateStr, boolean isAllDay, TimeZone timeZone) {
        Date date = DateUtil.parseDateTime(dateStr);
        return getDateTimeMap(date, isAllDay, timeZone);
    }

    public static Map<String, Object> getDateTimeMap(Date date, boolean isAllDay) {
        return getDateTimeMap(date, isAllDay, TimeZone.getDefault());
    }

    public static Map<String, Object> getDateTimeMap(Date date, boolean isAllDay, TimeZone timeZone) {
        Map<String, Object> res = Maps.newHashMap();
        res.put("timeZone", timeZone.toZoneId());
        if (isAllDay) {
            res.put("date", DateUtil.format(date, "yyyy-MM-dd"));
        } else {
            res.put("dateTime", DateUtil.format(date, "yyyy-MM-dd'T'HH:mm:ssXXX"));
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(JSON.toJSONString(getDateTimeMap("2022-08-22 12:30:00", false)));
    }

}
