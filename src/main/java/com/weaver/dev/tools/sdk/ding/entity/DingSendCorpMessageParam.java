package com.weaver.dev.tools.sdk.ding.entity;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingSendCorpMessageParam {

    private String agentId;
    private List<String> useridList;
    private List<String> deptIdList;
    private boolean toAllUser;
    private JSONObject msg;

}
