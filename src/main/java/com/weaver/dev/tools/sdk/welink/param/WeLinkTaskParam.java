package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WeLinkTaskParam {

    private String taskId;
    private String taskTitle;
    private String userId;
    private String userNameCn;
    private String userNameEn;
    private String detailsUrl;
    private String detailsUrlPc;
    private String appName;
    private String applicantUserId;
    private String applicantUserNameCn;
    private String applicantUserNameEn;
    private Integer isMsg;
    private Boolean isShowApplicantUserName;
    private String applicantId;
    private String businessCode;

}
