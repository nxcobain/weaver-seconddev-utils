package com.weaver.dev.tools.sdk.welink.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * welink 用户信息接口类
 */
@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WeLinkUserResult {

    private String code;
    private String message;
    private String userStatus;
    private String userId;
    private String corpUserId;
    private String userNameCn;
    private String userNameEn;
    private String sex;
    private String mobileNumber;
    private List<String> phoneNumber;
    private String mainCorpDeptCode;
    private String mainDeptCode;
    private List<String> corpDeptCodes;
    private List<String> deptCodes;
    private Map<String, Integer> orderInDepts;
    private Map<String, Integer> orderInCorpDepts;
    private String userEmail;
    private String avatar;
    private String employeeId;
    private String landlineNumber;
    private String businessAddress;
    private String baseLocation;
    private String position;
    private String corpSecretary;
    private String secretary;
    private String remark;
    private Integer isActivated;
    private Integer isAdmin;
    private String sipNum;
    private Integer isHidePhoneNumber;
    private Integer seniorMode;
    private List<String> roleIds;
    private Map<String, String> extAttr;
    private Date creationTime;
    private Date lastUpdatedTime;



}
