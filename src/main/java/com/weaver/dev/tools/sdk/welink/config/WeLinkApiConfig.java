package com.weaver.dev.tools.sdk.welink.config;

/**
 * WeLinkApi配置类
 */
public class WeLinkApiConfig {

    protected String protocol = "https";

    protected String host = "open.welink.huaweicloud.com";

    public WeLinkApiConfig() {
    }

    public WeLinkApiConfig(String protocol, String host) {
        this.protocol = protocol;
        this.host = host;
    }

    public String getProtocol() {
        return this.protocol;
    }

    public String getHost() {
        return this.host;
    }

    public String getHostUrl() {
        return String.format("%s://%s", getProtocol(), getHost());
    }

    public String getAccessTokenUrl() {
        return getHostUrl() + "/api/auth/v2/tickets";
    }

    public String getAttendanceRecords() {
        return getHostUrl() + "/api/attendance/v3/records";
    }

    public String getUserDetailUrl() {
        return getHostUrl() + "/api/contact/v2/user/detail";
    }

    public String getCommonCardUrl() {
        return getHostUrl() + "/api/messages/v2/card/common";
    }

    public String getAddTaskUrl() {
        return getHostUrl() + "/api/todo/v3/addtask";
    }

    public String getCardMessageUrl() {
        return getHostUrl() + "/api/messages/v1/card/wecode";
    }

    public String getCreateConferenceUrl() {
        return getHostUrl() + "/api/meeting/v1/createconference";
    }

    public String getCreateCalendarUrl() {
        return getHostUrl() + "/api/calendar/v1/events/add";
    }

}
