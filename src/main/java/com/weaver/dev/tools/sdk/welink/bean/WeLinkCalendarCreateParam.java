package com.weaver.dev.tools.sdk.welink.bean;

import cn.hutool.core.date.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WeLinkCalendarCreateParam {

    private List<String> receiverUserList;
    private String content;
    private String startTime;
    private String endTime;
    private String location;
    private Map<String, Object> remider;
    private String summary;

    public static String getTimeStamp(String dateStr) {
        return getTimeStamp(DateUtil.parseDateTime(dateStr));
    }

    public static String getTimeStamp(Date date) {
        return Long.toString(date.getTime());
    }

}
