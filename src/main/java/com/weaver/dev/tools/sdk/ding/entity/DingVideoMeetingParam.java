package com.weaver.dev.tools.sdk.ding.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingVideoMeetingParam {

    private String userId;
    private String confTitle;
    private List<String> inviteUserIds;
    private Boolean inviteCaller;

}
