package com.weaver.dev.tools.sdk.welink.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceRecordsBean {

    private String id;
    private String corpUserId;
    private String userId;
    private String userNameCn;
    private String deptName;
    private String groupName;
    private String punchTime;
    private String location;
    private String longitude;
    private String latitude;
    private String isRange;
    private String isField;
    private String remark;

}
