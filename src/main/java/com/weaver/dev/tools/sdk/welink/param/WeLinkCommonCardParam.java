package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WeLinkCommonCardParam {

    private String msgOwner;
    private String msgRange;
    private List<String> toUserList;
    private List<String> departmentList;
    private List<String> roleList;
    private String msgTitle;
    private String msgContent;
    private String receiveDeviceType;
    private String urlType;
    private String urlPath;
    private String desktopUrlPath;
    private String messageStatus;
    private String statusColor;
    private Integer isForceTips;

}
