package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 用户查询接口参数
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDetailParam {

    private String corpUserId;
    private String userId;
    private String mobileNumber;

}
