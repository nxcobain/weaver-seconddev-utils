package com.weaver.dev.tools.sdk.welink.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WeLinkMessageCardParam {

    private String publicAccID;
    private String msgRange;
    private List<String> toUserList;
    private List<String> departmentList;
    private List<String> roleList;
    private String msgTitle;
    private List<Map<String, String>> contentList;
    private String receiveDeviceType;
    private String urlType;
    private String urlPath;
    private String btnType;
    private List<Map<String, String>> btnList;
    private String messageStatus;
    private String statusColor;
    private Integer isForceTips;

}
