package com.weaver.dev.tools.sdk.ding.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingTaskResult {

    private String id;
    private String subject;
    private String description;
    private Long startTime;
    private Long dueTime;
    private Long finishTime;
    private Boolean done;
    private List<String> executorIds;
    private List<String> participantIds;
    private Map<String, Object> detailUrl;
    private String source;
    private String sourceId;
    private Long createdTime;
    private Long modifiedTime;
    private String creatorId;
    private String modifierId;
    private String bizTag;
    private String requestId;
    private Boolean isOnlyShowExecutor;
    private Integer priority;
    private Object notifyConfigs;

}
