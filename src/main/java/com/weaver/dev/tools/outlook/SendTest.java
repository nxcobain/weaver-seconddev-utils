package com.weaver.dev.tools.outlook;

import cn.hutool.core.date.DateUtil;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.service.ConflictResolutionMode;
import microsoft.exchange.webservices.data.core.enumeration.service.SendInvitationsOrCancellationsMode;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.MessageBody;

import java.net.URI;

public class SendTest {

    public static void main(String[] args) {
        ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);

        Appointment appointment;
        try {
            service.setCredentials(new WebCredentials("qffg1998@outlook.com", "qffg741852"));
            service.setUrl(new URI("https://" + "smtp.office365.com" + "/ews/exchange.asmx"));
            service.setTraceEnabled(true);
            appointment = new Appointment(service);
            //日程主题
            appointment.setSubject("测试主题31");
            appointment.setBody(new MessageBody("\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                    "    <meta content=\"text/html; charset=gb2312\">\n" +
                    "    <style type=\"text/css\">\n" +
                    "\tbody{\n" +
                    "\t  font-family:'微软雅黑';\n" +
                    "\t  font-size:16px;\n" +
                    "\t}\n" +
                    "\ttable\n" +
                    "    {\n" +
                    "\t table-layout: fixed;    \n" +
                    "     border-collapse:collapse;\n" +
                    "    }\n" +
                    "    table,th, td\n" +
                    "    {\n" +
                    "     border: 1px;\n" +
                    "    }\n" +
                    "\t.top{\n" +
                    "\t font-size:16px;\n" +
                    "\t background-color:#f4f4f4;\n" +
                    "\t}\n" +
                    "\t.bt{\n" +
                    "\t font-size:16px;\n" +
                    "\t \n" +
                    "\t}\n" +
                    "    </style>\n" +
                    "</head>\n" +
                    "<body>   \n" +
                    "   ${qxyy} <br>\n" +
                    "   各位领导，本次会议安排如下：<br>\n" +
                    "    会议名称：<b>${hymc}</b><br>\n" +
                    "    会议时间：<font color=\"red\">${hykssj}至${hyjssj}</font><br>\n" +
                    "    会议地点：${hys}; 电话会议：${dhhy}<br>\n" +
                    "\n" +
                    "    ${zw}\n" +
                    "    议程信息:\n" +
                    "  <table>\n" +
                    "    <tr class=\"top\">\n" +
                    "        <td >议题名称</td>\n" +
                    "        <td >发言人</td>\n" +
                    "        <td >议题内容</td>\n" +
                    "        <td >开始时间</td>\n" +
                    "        <td >结束时间</td>\n" +
                    "    </tr>\n" +
                    "    <#list dt1 as dt1>\n" +
                    "\t<tr class=\"bt\">\n" +
                    "\t\t<td >${dt1.ytmc}</td>\n" +
                    "\t\t<td >${dt1.fyrxsm}</td>\n" +
                    "\t\t<td >${dt1.ycnr}</td>\n" +
                    "\t\t<td >${dt1.kssj}</td>\n" +
                    "\t\t<td >${dt1.jssj}</td>\n" +
                    "\t</tr>\n" +
                    "    </#list>\n" +
                    "  </table>\n" +
                    "</br>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n"));
            appointment.setStart(DateUtil.parse("2022-07-05 15:00:00", "yyyy-MM-dd HH:mm:ss"));
            appointment.setEnd(DateUtil.parse("2022-07-05 16:00:00", "yyyy-MM-dd HH:mm:ss"));
            appointment.setLocation("会议室01");
            appointment.setIsAllDayEvent(false);
            appointment.save();

            appointment.getRequiredAttendees().add("qffg1998@outlook.com");
            appointment.update(ConflictResolutionMode.AutoResolve, SendInvitationsOrCancellationsMode.SendOnlyToAll);
            System.out.println(appointment.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
