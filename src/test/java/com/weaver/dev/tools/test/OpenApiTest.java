package com.weaver.dev.tools.test;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.weaver.dev.tools.openapi.WeaverOpenApi;
import com.weaver.dev.tools.openapi.config.WeaverOpenApiConfig;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ExecutionException;

public class OpenApiTest {

    @Test
    public void testMsg() throws ExecutionException {
        String appKey = "8f986579e31a9456ff905da071707b86";
        String appSecret = "75cc7e693d178b4231d9b260573570b5";
        String hostUrl = "https://api.yunteams.cn";
        String corpid = "fba4c42719388ab433252691850c367e";
        WeaverOpenApi weaverOpenApi = new WeaverOpenApi(appKey, appSecret, corpid, new WeaverOpenApiConfig() {
            @Override
            public String getHostUrl() {
                return hostUrl;
            }
        });

//        System.out.println(weaverOpenApi.accessToken());
//
//        System.out.println(weaverOpenApi.etLoginToken("18757432199"));
//        weaverOpenApi.queryRoomEvents("8514074616793847943", DateUtil.parse("2022-07-02 01:00:00", "yyyy-MM-dd HH:mm:ss").toJdkDate(), DateUtil.parse("2022-07-02 23:00:00", "yyyy-MM-dd HH:mm:ss").toJdkDate());
//        // 下载文件
////        /api/file/v2/common/download/{fileId}
//        weaverOpenApi.queryBlog(Lists.newArrayList("8514074616793847943"), DateUtil.parse("2022-07-02", "yyyy-MM-dd").toJdkDate(), DateUtil.parse("2022-07-02", "yyyy-MM-dd").toJdkDate());
        Map<String, Object> datamap = Maps.newHashMap();
        datamap.put("7835657919042873041", "是");
        JSONObject res = weaverOpenApi.workflowCreate("8514074616793847943",
                "744691228381798401", "短信通知-openapi创建", datamap);
        System.out.println(res.toJSONString());

        JSONObject queryRes = weaverOpenApi.getFieldIdById("8514074616793847943", "5955657918675533330");
        System.out.println(queryRes.toJSONString());
    }

}
